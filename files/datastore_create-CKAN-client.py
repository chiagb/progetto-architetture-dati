from ckanapi import RemoteCKAN
from pandas import DataFrame
output_table_1 = DataFrame()

#recupero la lista delle colonne
col_list = list(input_table_1.columns.values)
fields_list = []

#preparo il valore del parametro "resource"
package_id = {"package_id": "my_dataset_name5"}

#preparo il parametro fields a partire dalla lista delle colonne
for col in col_list:
	col_id = {"id":col}
	fields_list.append(col_id)

#setto l'user agent
ua = 'ckanapiexample/1.0 (+http://example.com/my/website)'

#chiamata vera e propria alla API. 
mysite = RemoteCKAN('http://localhost', apikey='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwQzlfRURYc004WDdzTWRaSV8xQlMwSlI0NkU0ZUllbGFQa3dBMWpQcksyYVFwMUNSRmFHOHh2MnljLVNrek1YbE9uZkRTMDJ2dkRocXdKeSIsImlhdCI6MTYyNTEzOTUzMn0.xWOmNnbwyFwesFWGM5zbD56iVyywS3yTKnAvD6V_0HA', user_agent=ua)
mysite.action.datastore_create(
    resource = package_id,
    fields=fields_list,
    records=input_table_1.to_dict('records') #conversione da DataFrame a Dictionary. Potrebbe metterci tanto tempo con grandi quantità di dati
    )