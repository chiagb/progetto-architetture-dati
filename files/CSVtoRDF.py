#importo le librerie necessarie
import numpy as np
import pandas as pd 
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD 
import urllib.parse 

#salvo la variabile in cui è contenuto il percorso del file
file_path=flow_variables['new variable']

#carico il file in un DataFrame
df=pd.read_csv(file_path,sep=",",quotechar='"')

#inizializzo la struttura che verrà popolata con il csv
g = Graph()

#inizializzo i namespace che utilizzerò per costruire le triple
ppl = Namespace('http://example.org/people/')
loc = Namespace('http://mylocations.org/addresses/')
schema = Namespace('http://schema.org/')

#per ogni riga, genero le triple attraverso le seguenti proprietà
for index, row in df.iterrows():
	# Persona con Nome -> tipo -> Persona
    g.add((URIRef(ppl+row['Name']), RDF.type, FOAF.Person))
    # Persona con Nome -> nome -> Literal (String)
    g.add((URIRef(ppl+row['Name']), URIRef(schema+'name'), Literal(row['Name'], datatype=XSD.string) ))
    # Persona con Nome -> età -> Literal (Integer)
    g.add((URIRef(ppl+row['Name']), FOAF.age, Literal(row['Age'], datatype=XSD.integer) ))
    # Persona con Nome -> indirizzo -> Literal (String)
    g.add((URIRef(ppl+row['Name']), URIRef(schema+'address'), Literal(row['Address'], datatype=XSD.string) ))
    # Indirizzo -> nome -> Literal (String)
    g.add((URIRef(loc+urllib.parse.quote(row['Address'])), URIRef(schema+'name'), Literal(row['Address'], datatype=XSD.string) ))

#verifico che l'rdf sia serializzato correttamente
print(g.serialize(format='turtle').decode('UTF-8'))

#genero il nome del file
filename=flow_variables['new variable (file name)'][0:-4]+".ttl"

#restituisco il nome del file come output del nodo
df=pd.DataFrame(columns=["filename"])
df.loc[0] = filename
output_table_1 = df

#salvo il file
g.serialize(filename,format='turtle')