from pandas import DataFrame
# Create empty table
output_table = DataFrame()

from ckanapi import RemoteCKAN, NotAuthorized
ua = 'ckanapiexample/1.0 (+http://example.com/my/website)'

mysite = RemoteCKAN('http://localhost', apikey='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwQzlfRURYc004WDdzTWRaSV8xQlMwSlI0NkU0ZUllbGFQa3dBMWpQcksyYVFwMUNSRmFHOHh2MnljLVNrek1YbE9uZkRTMDJ2dkRocXdKeSIsImlhdCI6MTYyNTEzOTUzMn0.xWOmNnbwyFwesFWGM5zbD56iVyywS3yTKnAvD6V_0HA', user_agent=ua)
mysite.action.package_create(name='my_dataset_name5', title='my_dataset_name5', owner_org='org-1')
