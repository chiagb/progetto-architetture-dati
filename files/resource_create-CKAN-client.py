from pandas import DataFrame
# Create empty table
output_table = DataFrame()

# Copy input to output
# output_table_1 = input_table_1.copy()

from ckanapi import RemoteCKAN
ua = 'ckanapiexample/1.0 (+http://example.com/my/website)'

mysite = RemoteCKAN('http://localhost', apikey='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwQzlfRURYc004WDdzTWRaSV8xQlMwSlI0NkU0ZUllbGFQa3dBMWpQcksyYVFwMUNSRmFHOHh2MnljLVNrek1YbE9uZkRTMDJ2dkRocXdKeSIsImlhdCI6MTYyNTEzOTUzMn0.xWOmNnbwyFwesFWGM5zbD56iVyywS3yTKnAvD6V_0HA', user_agent=ua)
mysite.action.resource_create(
    package_id='my_dataset_name5',
    url='localhost/dummy-value9',  # ignored but required by CKAN<2.6
    format='csv',
    name= 'res1',
    upload=open(flow_variables['csv-path'], 'rb')
    )
