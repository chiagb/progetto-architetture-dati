from pandas import DataFrame
# Creo una tabella vuota per l'output
output_table = DataFrame()

import requests

# qua vengono passati tutti i parametri della API (tranne i file), sotto forma di dictionary
data_dict = {
    "id":flow_variables['resource-id']
}

# questa è la richiesta vera e propria. 
resp = requests.post('http://localhost/api/action/resource_delete',
              data=data_dict,
              headers={"X-CKAN-API-Key": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwQzlfRURYc004WDdzTWRaSV8xQlMwSlI0NkU0ZUllbGFQa3dBMWpQcksyYVFwMUNSRmFHOHh2MnljLVNrek1YbE9uZkRTMDJ2dkRocXdKeSIsImlhdCI6MTYyNTEzOTUzMn0.xWOmNnbwyFwesFWGM5zbD56iVyywS3yTKnAvD6V_0HA"})
			  
#controllo che il codice della risposta sia corretto
assert resp.status_code == 200