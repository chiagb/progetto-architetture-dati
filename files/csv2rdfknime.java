// system imports
import org.knime.base.node.jsnippet.expression.AbstractJSnippet;
import org.knime.base.node.jsnippet.expression.Abort;
import org.knime.base.node.jsnippet.expression.Cell;
import org.knime.base.node.jsnippet.expression.ColumnException;
import org.knime.base.node.jsnippet.expression.TypeException;
import static org.knime.base.node.jsnippet.expression.Type.*;
import java.util.Date;
import java.util.Calendar;
import org.w3c.dom.Document;


// Your custom imports:
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.knime.core.node.NodeLogger;
// system variables
public class JSnippet extends AbstractJSnippet {
  // Fields for input columns
  /** Input column: "Path" */
  public String c_Path;
  /** Input column: "Path (#1)" */
  public String c_Path1;
  /** Input column: "name" */
  public String c_name;
  /** Input column: "name (#1)" */
  public String c_name1;

  // Fields for output columns
  /** Output column: "exitVal" */
  public Integer out_exitVal;

// Your custom variables:
String url="http://127.0.0.1:8890/DAV/home/demo/rdf_sink/";

// expression start
    public void snippet() throws TypeException, ColumnException, Abort {
// Enter your code here:
NodeLogger nl = NodeLogger.getLogger(AbstractJSnippet.class);
String file=c_Path;

String[] command = {"python", c_Path1, "-b","http://example.org/instances/","-p","http://example.org/property/","-d",",","-o",c_name1+c_name+".n3",c_Path};
//nl.warn(file);
try
{            
  final ProcessBuilder pb = new ProcessBuilder(command);
pb.redirectErrorStream(true);
Process process=pb.start();
BufferedReader inStreamReader = new BufferedReader(
    new InputStreamReader(process.getInputStream())); 
String line = null;
while((line = inStreamReader.readLine()) != null){
    nl.warn(line);
}
  int exitVal = process.exitValue();            
  nl.warn("Process exitValue: " + exitVal);
  out_exitVal = exitVal;
} catch (Exception e)
{
	
  nl.warn(e.getStackTrace()+" "+e.getMessage());
}






// expression end
    }
}
